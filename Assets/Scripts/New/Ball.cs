using UnityEngine;

public class Ball : MonoBehaviour
{
    [Range(0f, 10f)]
    public float AttractionForce;
    private Rigidbody rb;

    public Transform Owner;
    private bool m_IsOwned = false;
    private bool m_ShouldAttract = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {

    }


    public void OwnedBy(Transform parent)
    {
        rb.isKinematic = true;
        rb.transform.position = parent.position;
        rb.transform.SetParent(parent);
        m_IsOwned = true;
        Owner = parent;
    }

    public void Disown()
    {
        rb.isKinematic = false;
        rb.transform.SetParent(null);
        m_IsOwned = false;
        Owner = null;
    }

    public void Shoot(Vector3 target, float force)
    {
        if (!m_IsOwned) return;

        Disown();
        Vector3 dir = Vector3.Normalize(target - rb.position);

        rb.AddForce(dir * force, ForceMode.Impulse);
        //m_ShouldAttract = false;
    }

    public void Spawn(Vector3 pos)
    {
        rb.isKinematic = true;

        rb.position = pos;

        rb.isKinematic = false;
    }

}
