using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class Goaler : MonoBehaviour
{
    public TeamTag Team;

    private NavMeshAgent m_Agent;

    private Plane m_Plane;
    private Rigidbody m_Ballrb;
    private BallSocket m_BallSocket;

    private Vector3 m_InitPos;
    private void Awake()
    {
        m_BallSocket = GetComponentInChildren<BallSocket>();
        Team = GetComponent<TeamTag>();
        m_Agent = GetComponent<NavMeshAgent>();
        m_Plane = new Plane(transform.forward, transform.position);
    }


    private void OnEnable()
    {
        LevelManager.OnGameReset += OnGameReset;
    }

    private async void OnGameReset()
    {
        m_Agent.SetDestination(m_InitPos);
        await Task.Delay(3000);
        m_Agent.updateRotation = false;
        if (Team.Team == 1)
        {
            transform.LookAt(LevelManager.Instance.Team1Target);
        }
        else
        {
            transform.LookAt(LevelManager.Instance.Team2Target);
        }
        m_Agent.updateRotation = true;
    }

    private void OnDisable()
    {
        LevelManager.OnGameReset -= OnGameReset;
    }



    // Start is called before the first frame update
    void Start()
    {
        m_InitPos = transform.position;
        m_Ballrb = LevelManager.Instance.Ball.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        TryToCatchBall();

        if (Input.GetKeyDown(KeyCode.R))
        {
            Pass();
        }
    }

    public async void Pass()
    {


        Player player = null;
        float distance = 1000;
        foreach (var p in LevelManager.Instance.Players)
        {
            if (Vector3.Dot(transform.forward, Vector3.Normalize(p.transform.position - transform.position)) <= 0 || p.gameObject.GetComponent<TeamTag>().Team != Team.Team)
            {
                continue;
            }

            float dis = Vector3.Distance(transform.position, p.transform.position);
            if (dis < distance)
            {
                distance = dis;
                player = p;
            }

        }

        if (player == null) return;

        m_Agent.updatePosition = false;
        transform.LookAt(player.transform);
        m_Agent.updatePosition = true;

        await Task.Delay(1000);

        m_BallSocket.Shoot(player.transform.position, 20);

    }

    private void TryToCatchBall()
    {
        if (LevelManager.Instance.GetBallOwner() == null)
        {
            if (m_Ballrb != null)
            {
                Vector3 dir = Vector3.Normalize(m_Ballrb.velocity);
                Ray ray = new Ray(m_Ballrb.position, dir);
                if (m_Plane.Raycast(ray, out float dis))
                {
                    Vector3 pos = ray.GetPoint(dis);

                    float rnd = 0f;
                    pos.z += rnd;

                    if (Vector3.Distance(m_Ballrb.position, transform.position) < 10f)
                    {
                        m_Agent.SetDestination(pos);
                    }
                }
            }
        }
    }
}
