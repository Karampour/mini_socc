using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public bool IsAttacker;
    private NavMeshAgent m_Agent;
    public TeamTag TeamTag;
    private BallSocket m_BallSocket;
    private bool m_IsAttacking = false;

    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        TeamTag = GetComponent<TeamTag>();
        m_BallSocket = GetComponentInChildren<BallSocket>();
    }

    private void OnEnable()
    {
        LevelManager.OnGameReset += OnGameReset;
        LevelManager.OnAttacer += OnAttacker;
    }

    private void OnAttacker(int team)
    {
        if (team == TeamTag.Team)
        {
            m_IsAttacking = true;
        }
        else
        {
            m_IsAttacking = false;
        }
    }

    private async void OnGameReset()
    {
        await Task.Delay(1000);
        GoToland();
    }

    private void OnDisable()
    {
        LevelManager.OnGameReset -= OnGameReset;
        LevelManager.OnAttacer -= OnAttacker;
    }

    // Start is called before the first frame update
    void Start()
    {
        GoToland();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        Ball ball = collision.gameObject.GetComponent<Ball>();
        if (ball != null)
        {
            ball.OwnedBy(m_BallSocket.transform);
            m_BallSocket.Own(ball);

            Pass();
        }
    }

    public void GoToland()
    {
        Vector3 rnd = new Vector3(Random.Range(-2, 3), 0, Random.Range(-2, 3));
        Vector3 pos;

        if (m_IsAttacking && IsAttacker)
        {
            if (TeamTag.Team == 1)
            {
                pos = LevelManager.Instance.GetLand(2);
            }
            else
            {
                pos = LevelManager.Instance.GetLand(1);
            }
        }
        else
        {
            if (TeamTag.Team == 1)
            {
                pos = LevelManager.Instance.GetLand(1);
            }
            else
            {
                pos = LevelManager.Instance.GetLand(2);
            }
        }

        m_Agent.SetDestination(pos + rnd);
    }

    public async void Pass()
    {

        await Task.Delay(1000);

        if (TeamTag.Team == 1)
        {
            m_Agent.updatePosition = false;
            transform.LookAt(LevelManager.Instance.Team1Target);
            m_Agent.updatePosition = true;
        }
        else
        {
            m_Agent.updatePosition = false;
            transform.LookAt(LevelManager.Instance.Team2Target);
            m_Agent.updatePosition = true;
        }

        Player player = null;
        float distance = 1000;
        foreach (var p in LevelManager.Instance.Players)
        {
            if (Vector3.Dot(transform.forward, Vector3.Normalize(p.transform.position - transform.position)) <= 0.7f || p.gameObject.GetComponent<TeamTag>().Team != TeamTag.Team)
            {
                continue;
            }

            float dis = Vector3.Distance(transform.position, p.transform.position);
            if (dis < distance)
            {
                distance = dis;
                player = p;
            }

        }

        if (player == null)
        {
            TryToScore();

            return;
        }

        m_Agent.updatePosition = false;
        transform.LookAt(player.transform);
        m_Agent.updatePosition = true;

        await Task.Delay(1000);

        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.SphereCast(ray, 0.5f, out RaycastHit hit))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red, 2f);

            Player p = hit.collider.GetComponent<Player>();
            if (p != null)
            {
                if (p.TeamTag.Team == TeamTag.Team)
                {
                    m_BallSocket.Shoot(player.transform.position, 20);
                }
            }
        }

    }

    public async void TryToScore()
    {
        await Task.Delay(1000);
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red, 2f);

            Player p = hit.collider.GetComponent<Player>();
            if (p != null)
            {
                LevelManager.Instance.ResetGame();
            }
            else
            {
                if (TeamTag.Team == 1)
                {
                    m_BallSocket.Shoot(LevelManager.Instance.Team1Target.position, 20);
                }
                else
                {
                    m_BallSocket.Shoot(LevelManager.Instance.Team2Target.position, 20);
                }
            }
        }
        else
        {
            if (TeamTag.Team == 1)
            {
                m_BallSocket.Shoot(LevelManager.Instance.Team1Target.position, 20);
            }
            else
            {
                m_BallSocket.Shoot(LevelManager.Instance.Team2Target.position, 20);
            }
        }
    }
}
