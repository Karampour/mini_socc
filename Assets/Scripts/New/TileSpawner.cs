using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public Vector3[,] Tiles = new Vector3[3, 6];
    public float ZSpan;
    public float XSpan;

    public Vector3[,] Team1Tiles = new Vector3[3, 3];
    public Vector3[,] Team2Tiles = new Vector3[3, 3];


    private void Awake()
    {
        for (int z = 0; z < 3; z++)
        {
            for (int x = 0; x < 6; x++)
            {
                Tiles[z, x] = transform.position + new Vector3(XSpan * x, transform.position.y, ZSpan * z);

                if (x > 2)
                {
                    Team2Tiles[z, x - 3] = Tiles[z, x];
                }
                else
                {
                    Team1Tiles[z, x] = Tiles[z, x];
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (var tile in Tiles)
        {
            Gizmos.DrawWireSphere(tile, 0.5f);
        }
    }
}
