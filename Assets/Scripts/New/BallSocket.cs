using System.Threading.Tasks;
using UnityEngine;

public class BallSocket : MonoBehaviour
{

    private Ball m_Ball;


    private bool m_ShouldCatch = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public async void DeactiveSocket(int cooldown)
    {
        m_ShouldCatch = false;
        await Task.Delay(cooldown);
        m_ShouldCatch = true;
    }

    public void Shoot(Vector3 target, float force)
    {
        if (m_Ball == null) return;

        //DeactiveSocket(300);
        m_Ball.Shoot(target, force);
        m_Ball = null;

    }

    public void Own(Ball ball)
    {
        m_Ball = ball;
    }
}
