using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : GenericSingletonClass<LevelManager>
{
    public Ball Ball;
    public BallSocket[] Goalkeepers = new BallSocket[2];
    public TileSpawner TileSpawner;
    public Player[] Players = new Player[8];

    private List<Vector3> m_Team1Lands = new List<Vector3>();
    private List<Vector3> m_Team2Lands = new List<Vector3>();

    public Transform Team1Target;
    public Transform Team2Target;

    public static Action OnGameReset;
    public static Action<int> OnAttacer;

    private float m_timer = 0;
    private new void Awake()
    {
        base.Awake();

        Players = FindObjectsOfType<Player>();
    }

    // Start is called before the first frame update
    void Start()
    {
        SpawnBall(0, true);
        ResetLands();


    }

    // Update is called once per frame
    void Update()
    {
        BallTimer();

        //debug
        if (Input.GetKeyDown(KeyCode.T))
        {
            ResetGame();
        }
    }



    public Transform GetBallOwner()
    {
        return Ball.Owner;
    }


    public void ResetLands()
    {
        foreach (var pos in TileSpawner.Team1Tiles)
        {
            m_Team1Lands.Add(pos);
        }
        foreach (var pos in TileSpawner.Team2Tiles)
        {
            m_Team2Lands.Add(pos);
        }
    }

    public Vector3 GetLand(int team)
    {
        if (team == 1)
        {
            int rnd = UnityEngine.Random.Range(0, m_Team1Lands.Count);
            Vector3 pos = m_Team1Lands[rnd];
            m_Team1Lands.RemoveAt(rnd);
            return pos;
        }
        else
        {
            int rnd = UnityEngine.Random.Range(0, m_Team2Lands.Count);
            Vector3 pos = m_Team2Lands[rnd];
            m_Team2Lands.RemoveAt(rnd);
            return pos;
        }
    }

    public void SpawnBall(int team, bool isRandom)
    {
        if (isRandom)
        {
            int rnd = UnityEngine.Random.Range(0, 2);
            Ball.Spawn(Goalkeepers[rnd].transform.position);
            Ball.OwnedBy(Goalkeepers[rnd].transform);
            Goalkeepers[rnd].Own(Ball);

            OnAttacer?.Invoke(rnd + 1);
        }
        else
        {
            Ball.Spawn(Goalkeepers[team - 1].transform.position);
            Ball.OwnedBy(Goalkeepers[team - 1].transform);
            Goalkeepers[team - 1].Own(Ball);
            OnAttacer?.Invoke(team);
        }
    }

    public void ResetGame()
    {
        ResetLands();
        SpawnBall(0, true);
        OnGameReset?.Invoke();
    }


    public void BallTimer()
    {
        if (Ball.Owner == null)
        {
            m_timer += Time.deltaTime;
        }
        else
        {
            m_timer = 0f;
        }

        if (m_timer >= 3f)
        {
            ResetGame();
        }
    }
}
